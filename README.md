# pjsip

#### 介绍
pjsip2.5.5 for Android && Atelier

#### 编译

##### Android:

```
export APP_PLATFORM=android-21
export TARGET_ABI=arm64-v8a
export ANDROID_NDK_ROOT=/home/lxj/android-ndk-r13b

./configure-android --use-ndk-cflags
make dep && make
```

##### Atelier:

```
export APP_PLATFORM=android-24
export TARGET_ABI=arm64-v8a
export ANDROID_NDK_ROOT=/home/lxj/emss-ndk

./configure-atelier --use-ndk-cflags
make dep  && make
```