#!/bin/bash

PJLIB_PATH="./lib/$1/pjlib/*"
PJLIB_UTIL_PATH="./lib/$1/pjlib-util/*"
PJMEDIA_PATH="./lib/$1/pjmedia/*"
PJNATH_PATH="./lib/$1/pjnath/*"
PJSIP_PATH="./lib/$1/pjsip/*"
THIRD_PARTY="./lib/$1/third-party/*"
PJAPPS_PATH="./lib/$1/pjsip-apps/*"

echo "start coping to $1"

rm -rf $PJLIB_PATH
rm -rf $PJLIB_UTIL_PATH
rm -rf $PJMEDIA_PATH
rm -rf $PJNATH_PATH
rm -rf $PJSIP_PATH
rm -rf $THIRD_PARTY
rm -rf $PJAPPS_PATH

rm -rf ./pjlib/lib/*
rm -rf ./pjlib-util/lib/*
rm -rf ./pjmedia/lib/*
rm -rf ./pjnath/lib/*
rm -rf ./pjsip/lib/*
rm -rf ./third_party/lib/*
rm -rf ./pjsip-apps/lib/*



echo "copy done"
