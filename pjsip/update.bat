cp ./pjlib/lib/* ../pjsip/libs/pjlib/
cp ./pjlib-util/lib/* ../pjsip/libs/pjlib-util/
cp ./pjmedia/lib/* ../pjsip/libs/pjmedia/
cp ./pjnath/lib/* ../pjsip/libs/pjnath/
cp ./pjsip/lib/* ../pjsip/libs/pjsip/
cp ./third_party/lib/* ../pjsip/libs/third-party/

xcopy.exe  pjlib\include\*.* D:\Android\plugin\pjsip\pjsip\include\pjlib /s
xcopy.exe  pjlib-util\include\*.* D:\Android\plugin\pjsip\pjsip\include\pjlib-util /s
xcopy.exe  pjmedia\include\*.* D:\Android\plugin\pjsip\pjsip\include\pjmedia /s
xcopy.exe  pjnath\include\*.* D:\Android\plugin\pjsip\pjsip\include\pjnath /s
xcopy.exe  pjsip\include\*.* D:\Android\plugin\pjsip\pjsip\include\pjsip /s

pause
